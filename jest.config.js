module.exports = {
  verbose: false,
  transform: {
    '^.+\\.(t|j)s$': 'ts-jest',
  },
  roots: ['<rootDir>'],
  // testMatch: [
  //   '**/test/**/*test*.js',
  //   '**/*test*.js',
  //   '!**/mocha/**',
  //   '!**/playground/**',
  //   '!**/*test-helper*',
  //   '!**/*anti-pattern*', // Uncomment this only when you want to inspect the consequences of anti-patterns
  //   '!**/*performance*' //Uncomment this only when you want to inspect the performance of tests
  // ],
  collectCoverage: false,
  coverageReporters: ['text-summary', 'lcov'],
  collectCoverageFrom: ['**/*.js', '!**/node_modules/**', '!**/test/**'],
  forceExit: true,
  testEnvironment: 'node',
  notify: true,
  globalSetup: './test-setup/global-setup.ts',
  globalTeardown: './test-setup/global-teardown.ts',
  notifyMode: 'change',
  moduleFileExtensions: ['js', 'json', 'ts'],
};
