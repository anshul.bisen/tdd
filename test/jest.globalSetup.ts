import { MongoMemoryServer } from 'mongodb-memory-server';
import { MONGO_PORT } from './constants';

module.exports = async () => {
  global.__MONGOD__ = await MongoMemoryServer.create({
    instance: {
      port: MONGO_PORT,
    },
  });
};
