import { Test, TestingModule } from '@nestjs/testing';

export const createTestContext = async (imports, providers, controllers) => {
  const module: TestingModule = await Test.createTestingModule({
    imports,
    providers,
    controllers,
  }).compile();
  const app = module.createNestApplication();

  return { app };
};
