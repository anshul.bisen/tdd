import { execSync } from 'child_process';
import isReachable from 'is-reachable';
import { MongoMemoryReplSet } from 'mongodb-memory-server';

export default async () => {
  console.time('Initiating jest setup...');

  const [stageArg] = process.argv.filter((x) => x.startsWith('--stage='));
  const stage = stageArg ? stageArg.split('=')[1] : 'dev';
  console.log(`Testing environment value found: ${stage}`);

  const [deploymentArgs] = process.argv.filter((x) =>
    x.startsWith('--dbType='),
  );
  const deployment = deploymentArgs ? deploymentArgs.split('=')[1] : null;
  console.log(`Deployment value found: ${deployment}`);

  switch (deployment) {
    case 'memory':
      await setupMemoryDbs();
      break;
    case 'custom':
      /** do nothing */
      break;
    case 'docker':
      await setupDockerDbs();
      break;
    default:
      const isDockerInstalled = execSync('docker --version').toString();
      if (isDockerInstalled.includes('command not found')) {
        console.log(
          'Docker is not installed, starting mongo in memory server instead...',
          isDockerInstalled,
        );
        await setupMemoryDbs();
        break;
      }

      // const isDockerRunning = execSync('docker stats --no-stream >/dev/null 2>&1').toString()
      // if(!isDockerRunning){
      //   console.log("Docker is not running, starting mongo in memory server instead...", isDockerRunning)
      //   await setupMemoryDbs()
      //   break
      // }

      await setupDockerDbs();
      const isDBReachable = await isReachable('http://localhost:27021');
      // if Db hosted by docker is not reachable start in memory server for testing
      if (!isDBReachable) {
        console.log(
          'Docker DB instance not reachable, starting mongo in memory server instead...',
        );
        await setupMemoryDbs();
      }
  }

  process.env.TEST_DB_NAME = 'quillbot';
  // ️️️✅ Best Practice: Speed up during development, if already live then do nothing
  try {
    // execSync('npm run test-db:migrate');
  } catch (error) {
    console.log('Error occured while migrating seed data', error);
  }

  // 👍🏼 We're ready
  console.timeEnd('jest have been initialised');
};

async function setupDockerDbs() {
  console.log('setting up dbs in docker...');
  let isDBReachable = await isReachable('http://localhost:27021');
  if (!isDBReachable) {
    console.log('setting up mongo replica set');
    execSync('./test-setup/startdb.sh');
  }

  isDBReachable = await isReachable('http://localhost:27021');
  if (isDBReachable) {
    process.env.TEST_DB_URL = 'mongodb://localhost:27021';
    console.log(`Db is reachable on: ${process.env.TEST_DB_URL}`);
  } else {
    console.log('Db is not reachable on docker...');
  }
}

async function setupMemoryDbs() {
  console.log('setting up dbs in memory...');
  const replSet = new MongoMemoryReplSet({
    replSet: { storageEngine: 'wiredTiger' },
  });
  await replSet.start();
  const uri = await replSet.getUri();
  process.env.TEST_DB_URI = uri;
  const [dbUrl] = uri.split('/?');

  process.env.TEST_DB_URL = dbUrl;
  global.__TEST_MONGO__ = replSet;
  console.log('Mongo memory URI is', uri);
}
