import { Test } from '@nestjs/testing';
import {
  Controller,
  Get,
  INestApplication,
  MiddlewareConsumer,
  Module,
  Req,
} from '@nestjs/common';
import * as request from 'supertest';

import { TraceMiddleware } from './trace.middleware';

@Controller()
class TestController {
  @Get('trace')
  test(@Req() request) {
    return request['UNIQUE_ID'];
  }
}

@Module({})
export class ApplicationModule {}

@Module({
  imports: [ApplicationModule],
  controllers: [TestController],
})
class TestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(TraceMiddleware).forRoutes('trace');
  }
}

describe('TraceMiddleware', () => {
  let app: INestApplication;

  beforeEach(async () => {
    app = (
      await Test.createTestingModule({
        imports: [TestModule],
      }).compile()
    ).createNestApplication();

    await app.init();
  });

  it(`should return a number on visiting "/trace" endpoint`, () => {
    // Arrange
    const server = app.getHttpServer();

    // Act
    const response = request(server).get('/trace');

    // Assert
    return response.expect(200, /\d+/);
  });

  afterEach(async () => {
    await app.close();
  });
});
