import { Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';

@Injectable()
export class TraceMiddleware implements NestMiddleware {
  use(request: Request, response: Response, next: NextFunction) {
    request['UNIQUE_ID'] = Math.floor(Math.random() * 10000) + 1;
    next();
  }
}
