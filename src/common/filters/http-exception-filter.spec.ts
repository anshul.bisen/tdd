import { HttpExceptionFilter } from './http-exception-filter';
import { HttpException, HttpStatus } from '@nestjs/common';

describe('HttpExceptionFilter', () => {

  beforeEach(() => {

  });

  it('should add timestamp and path in the response body', () => {
    //Arrange
    const filter = new HttpExceptionFilter();
    const url = '/api/test';
    const mockStatus = jest.fn();
    const mockJson = jest.fn();
    const mockGetRequest = jest.fn().mockImplementation(() => ({ url }));
    const mockGetResponse = jest.fn().mockImplementation(() => ({
      status: mockStatus,
      json: mockJson,
    }));
    const mockHttpArgumentsHost = jest.fn().mockImplementation(() => ({
      getResponse: mockGetResponse,
      getRequest: mockGetRequest,
    }));
    const mockArgumentsHost = {
      switchToHttp: mockHttpArgumentsHost,
      getArgByIndex: jest.fn(),
      getArgs: jest.fn(),
      getType: jest.fn(),
      switchToRpc: jest.fn(),
      switchToWs: jest.fn(),
    };

    //Act
    filter.catch(
      new HttpException('Http exception', HttpStatus.BAD_REQUEST),
      mockArgumentsHost,
    );

    //Assert
    expect(mockGetResponse).toHaveBeenCalled();
    expect(mockStatus).toBeCalledWith(HttpStatus.BAD_REQUEST);
    expect(mockJson).toBeCalledWith({
      timestamp: expect.any(String),
      path: url,
      message: 'Http exception',
    });
  });
});
