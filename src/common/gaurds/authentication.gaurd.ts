import {
  ExecutionContext,
  Injectable,
  CanActivate,
  UnauthorizedException,
} from '@nestjs/common';
import type { Request } from 'express';

@Injectable()
export class AuthenticatedGuard implements CanActivate {
  canActivate(context: ExecutionContext): true | never {
    const req = context.switchToHttp().getRequest<Request>();
    const isAuthenticated = req.get.arguments.isAuthenticated();
    if (!isAuthenticated) {
      throw new UnauthorizedException();
    }
    return isAuthenticated;
  }
}
