import { createMock } from '@golevelup/ts-jest';
import { ExecutionContext, UnauthorizedException } from '@nestjs/common';

import { AuthenticatedGuard } from './authentication.gaurd';

describe('AuthenticatedGuard', () => {
  let authenticatedGuard: AuthenticatedGuard;

  beforeEach(() => {
    authenticatedGuard = new AuthenticatedGuard();
  });

  it('should be defined', () => {
    expect(authenticatedGuard).toBeDefined();
  });

  describe('canActivate', () => {
    it('should return true when user is authenticated', () => {
      // Arrange
      const mockContext = createMock<ExecutionContext>();

      // Act
      mockContext.switchToHttp().getRequest.mockReturnValue({
        // method attached to `req` instance by Passport lib
        get: {
          arguments: {
            isAuthenticated: () => true,
          },
        },
      });

      const canActivate = authenticatedGuard.canActivate(mockContext);

      // Assert
      expect(canActivate).toBe(true);
    });

    it('should thrown an Unauthorized (HTTP 401) error when user is not authenticated', () => {
      // Arrange
      const mockContext = createMock<ExecutionContext>();

      // Act
      mockContext.switchToHttp().getRequest.mockReturnValue({
        // method attached to `req` instance by Passport lib
        get: {
          arguments: {
            isAuthenticated: () => false,
          },
        },
      });

      const callCanActivate = () => authenticatedGuard.canActivate(mockContext);

      // Assert
      expect(callCanActivate).toThrowError(UnauthorizedException);
    });
  });
});
