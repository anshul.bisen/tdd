import { Test, TestingModule } from '@nestjs/testing';
import { MongooseModule } from '@nestjs/mongoose';

import { EmployeeRepository } from './employee.repository';
import { MONGO_PORT } from '../../test/constants';
import { CreateEmployeeDto } from './dto/create-employee.dto';
import { Employee, EmployeeSchema } from './model/employee';

describe('EmployeeRepository', () => {
  let employeeRepository: EmployeeRepository;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        MongooseModule.forRoot(`mongodb://localhost:${MONGO_PORT}`),
        MongooseModule.forFeature([
          { name: Employee.name, schema: EmployeeSchema },
        ]),
      ],
      providers: [EmployeeRepository],
    }).compile();

    employeeRepository = module.get<EmployeeRepository>(EmployeeRepository);
  });

  afterEach(async () => {
    await employeeRepository.deleteMany();
  });

  it('should create employee and return new id and createdAt', async () => {
    //Arrange
    const createEmployeeDto = new CreateEmployeeDto();
    createEmployeeDto.name = 'SHIELD';
    createEmployeeDto.post = 'manager';
    createEmployeeDto.departmentId = 'D1';

    //Act
    const employee: Employee = await employeeRepository.save(createEmployeeDto);

    //Assert
    expect(employee.name).toBe(createEmployeeDto.name);
    expect(employee.createdAt).toBeDefined();
  });

  it('should return array of existing employees by departmentId', async () => {
    //Arrange
    const createEmployeeDto = new CreateEmployeeDto();
    createEmployeeDto.name = 'SHIELD';
    createEmployeeDto.post = 'manager';
    createEmployeeDto.departmentId = 'D1';

    const employee: Employee = await employeeRepository.save(createEmployeeDto);

    //Act
    const employees: Employee[] = await employeeRepository.find(
      createEmployeeDto.departmentId,
    );

    //Assert
    expect(employees.length).toBe(1);
    expect(employees[0].name).toBe(createEmployeeDto.name);
  });
});
