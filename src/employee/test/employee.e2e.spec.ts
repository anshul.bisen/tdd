import supertest from 'supertest';
import { INestApplication, HttpStatus } from '@nestjs/common';
import mongoose from 'mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import { MongooseModule } from '@nestjs/mongoose';
import { CreateEmployeeDto } from '../dto/create-employee.dto';

import {
  providers,
  COLLECTION_MAPPING,
  mongoModelMap,
} from './dependency_provider';
import { EmployeeController } from '../employee.controller';

describe('Departments', () => {
  let app: INestApplication;
  let server;
  const testDbUrl = `${process.env.TEST_DB_URL}/${process.env.TEST_DB_NAME}?directConnection=true`;

  beforeAll(async () => {
    await mongoose.connect(testDbUrl);
  });

  beforeEach(async () => {
    const schemaDefinitions = Object.values(COLLECTION_MAPPING).map(
      (schemaDef) => ({ name: schemaDef.name, schema: schemaDef.schema }),
    );

    /** create testing environment context*/
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        MongooseModule.forRoot(testDbUrl, {
          useNewUrlParser: true,
          useUnifiedTopology: true,
        }),
        MongooseModule.forFeature(schemaDefinitions),
      ],
      providers,
      controllers: [EmployeeController],
    }).compile();
    app = module.createNestApplication();
    await app.init();
    server = app.getHttpServer();

    /**
     * add seed data before each test, this should only load/set data for test in the describe block
     * the data loaded must be unloaded/unsetted after each test
     */
    const promises = [];
    promises.push(
      mongoModelMap
        .get('EmployeeModel')
        .create({ departmentId: 'Avengers', name: 'Iron Man' }),
    );
    promises.push(
      mongoModelMap
        .get('EmployeeModel')
        .create({ departmentId: 'Avengers', name: 'Dr. Starnge' }),
    );

    await Promise.all(promises);
  });

  afterEach(async () => {
    const promises = [];
    promises.push(
      mongoModelMap.get('EmployeeModel').remove({ name: 'Avengers' }),
    );
    await Promise.all(promises);
    /** server should be closed after each as a best practice */
  });

  it(`/POST employee`, async () => {
    //Arrange
    const createEmployeeDto = new CreateEmployeeDto();
    createEmployeeDto.name = 'Ethan Hunt';
    createEmployeeDto.post = 'Agent';
    createEmployeeDto.departmentId = 'IMF';

    //Act
    const response = await supertest(server)
      .post('/employee')
      .send(createEmployeeDto);

    //Assert
    expect(response).toEqual(
      expect.objectContaining({ status: HttpStatus.CREATED }),
    );

    // check if data has been saved
    //Act
    const verificationResponse = await supertest(server).get(
      '/employee/departments/IMF',
    );

    //Assert
    expect(verificationResponse).toEqual(
      expect.objectContaining({
        status: HttpStatus.OK,
        /** body should be an array  */
        body: expect.arrayContaining([
          /** array should have object, with name property, the value should be string */
          expect.objectContaining({
            departmentId: 'IMF',
            post: 'Agent',
            name: 'Ethan Hunt',
          }),
        ]),
      }),
    );
  });

  it(`/GET empolyees by departmentId`, async () => {
    //Arrange

    //Act
    const request = await supertest(server).get(
      '/employee/departments/Avengers',
    );

    //Assert
    expect(request).toEqual(
      expect.objectContaining({
        status: HttpStatus.OK,
        /** body should be an array  */
        body: expect.arrayContaining([
          /** array should have object, with name property, the value should be string */
          expect.objectContaining({ name: expect.any(String) }),
        ]),
      }),
    );
  });

  afterAll(async () => {
    server.close();
    await app.close();
  });
});
