import { EmployeeService } from '../../employee/employee.service';
import { EmployeeRepository } from '../../employee/employee.repository';
import { Employee, EmployeeSchema } from '../../employee/model/employee';
import mongoose from 'mongoose';
import { getModelToken } from '@nestjs/mongoose';

export const mongoModels = [
  {
    provide: getModelToken('Employee'),
    useValue: mongoose.model('Employee', EmployeeSchema),
  },
];

/** any injectable should come here. example services, models, context, etc. */
export const providers = [EmployeeService, EmployeeRepository, ...mongoModels];

export const COLLECTION_MAPPING = {
  employee: {
    collection: Employee,
    schema: EmployeeSchema,
    name: Employee.name,
  },
};

export const mongoModelMap = new Map();
mongoModels.forEach((model) =>
  mongoModelMap.set(model.provide, model.useValue),
);
