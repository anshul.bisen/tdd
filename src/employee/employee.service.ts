import { Injectable } from '@nestjs/common';
import axios from 'axios';

import { CreateEmployeeDto } from './dto/create-employee.dto';
import { EmployeeRepository } from './employee.repository';
import { Employee } from './model/employee';

@Injectable()
export class EmployeeService {
  constructor(private readonly employeeRepository: EmployeeRepository) {}

  async save(createEmployeeDto: CreateEmployeeDto): Promise<Employee> {
    return this.employeeRepository.save(createEmployeeDto);
  }

  async find(departmentId: string): Promise<Employee[]> {
    return this.employeeRepository.find(departmentId);
  }

  async findByName(name: string): Promise<Employee[]> {
    const { data } = await axios.get(
      `http://some-test-end-point.com/employee/${name}/alterEgo/name`,
    );
    return data;
  }
}
