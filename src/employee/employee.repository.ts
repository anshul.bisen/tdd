import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';

import { CreateEmployeeDto } from './dto/create-employee.dto';
import { Employee, EmployeeDocument } from './model/employee';

@Injectable()
export class EmployeeRepository {
  constructor(
    @InjectModel(Employee.name)
    private employeeModel: Model<EmployeeDocument>,
  ) {}

  async save(createEmployeeDto: CreateEmployeeDto): Promise<Employee> {
    const createdEmployee = new this.employeeModel(createEmployeeDto);
    createdEmployee.createdAt = new Date();
    return createdEmployee.save();
  }

  async find(departmentId: string): Promise<Employee[]> {
    return this.employeeModel.find({ departmentId }).exec();
  }

  async deleteMany(): Promise<void> {
    await this.employeeModel.deleteMany({}).exec();
  }
}
