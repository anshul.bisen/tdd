import {
  ArgumentMetadata,
  BadRequestException,
  Injectable,
  PipeTransform,
} from '@nestjs/common';
import * as Joi from 'joi';
import { CreateDepartmentDto } from '../dto/create-department.dto';

@Injectable()
export class CreateDepartmentDtoPipe implements PipeTransform {
  transform(value: CreateDepartmentDto, metadata: ArgumentMetadata) {
    const schema = Joi.object({
      name: Joi.string().required(),
    });

    const { error } = schema.validate(value);
    if (error) {
      throw new BadRequestException();
    }
    return value;
  }
}
