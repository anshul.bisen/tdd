import { CreateDepartmentDtoPipe } from './create-department-dto.pipe';
import { CreateDepartmentDto } from '../dto/create-department.dto';
import { BadRequestException } from '@nestjs/common';

describe('CreateDepartmentDtoPipe', () => {
  let pipe: CreateDepartmentDtoPipe;

  beforeEach(() => {
    pipe = new CreateDepartmentDtoPipe();
  });

  it('should convert create department request to department', () => {
    //Arrange
    const createDepartmentRequest = {
      name: 'SHIELD',
    };

    const expectedDepartment = new CreateDepartmentDto();
    expectedDepartment.name = 'SHIELD';

    //Act
    const transformedDepartment: CreateDepartmentDto = pipe.transform(
      createDepartmentRequest,
      null,
    );

    //Assert
    expect(transformedDepartment).toEqual(expectedDepartment);
  });

  it('should throw BadRequestException if name is empty/blank', () => {
    //Arrange
    const createDepartmentDto = {
      name: '',
    };

    //Act
    const result = () => pipe.transform(createDepartmentDto, null);

    //Assert
    expect(result).toThrow(BadRequestException);
  });
});
