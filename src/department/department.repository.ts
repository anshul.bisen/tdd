import { Injectable } from '@nestjs/common';
import { CreateDepartmentDto } from './dto/create-department.dto';
import { Department, DepartmentDocument } from './model/department';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

@Injectable()
export class DepartmentRepository {
  constructor(
    @InjectModel(Department.name)
    private departmentModel: Model<DepartmentDocument>,
  ) {}

  async save(createDepartmentDto: CreateDepartmentDto): Promise<Department> {
    const createdDepartment = new this.departmentModel(createDepartmentDto);
    createdDepartment.createdAt = new Date();
    return createdDepartment.save();
  }

  findById(departmentId: string): Promise<Department> {
    return this.departmentModel.findById(departmentId).exec();
  }

  findByName(departmentName: string): Promise<Department[]> {
    return this.departmentModel.find({ name: departmentName }).exec();
  }
}
