import { Injectable } from '@nestjs/common';
import { CreateDepartmentDto } from './dto/create-department.dto';
import { DepartmentRepository } from './department.repository';
import { Department } from './model/department';
import { DepartmentWithEmployees } from './model/department-with-employees';
import { EmployeeService } from '../employee/employee.service';
import { Employee } from '../employee/model/employee';
import axios from 'axios';

@Injectable()
export class DepartmentService {
  constructor(
    private readonly departmentRepository: DepartmentRepository,
    private readonly employeeService: EmployeeService,
  ) {}

  save(createDepartmentDto: CreateDepartmentDto): Promise<Department> {
    return this.departmentRepository.save(createDepartmentDto);
  }

  findById(departmentId: string): Promise<Department> {
    return this.departmentRepository.findById(departmentId);
  }

  findByName(departmentName: string): Promise<Department[]> {
    return this.departmentRepository.findByName(departmentName);
  }

  async getHodOfDepartment(departmentName: string): Promise<string> {
    const { data } = await axios.get(
      `http://some-test-end-point.com/department/${departmentName}/hod`,
    );
    return data;
  }

  async findByIdWithEmployees(
    departmentId: string,
  ): Promise<DepartmentWithEmployees> {
    const department: Department = await this.departmentRepository.findById(
      departmentId,
    );

    const employees: Employee[] = await this.employeeService.find(departmentId);

    return DepartmentService.convert(department, employees);
  }

  private static convert(
    department: Department,
    employees: Employee[],
  ): DepartmentWithEmployees {
    return {
      ...department,
      employees,
    };
  }
}
