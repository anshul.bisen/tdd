import { Test, TestingModule } from '@nestjs/testing';
import { DepartmentService } from './department.service';
import { CreateDepartmentDto } from './dto/create-department.dto';
import { DepartmentRepository } from './department.repository';
import { EmployeeService } from '../employee/employee.service';
import { Department } from './model/department';
import { Employee } from '../employee/model/employee';
import { DepartmentWithEmployees } from './model/department-with-employees';

jest.mock('./department.repository');
jest.mock('./../employee/employee.service');

describe('DepartmentService', () => {
  let service: DepartmentService;
  let repository: DepartmentRepository;
  let employeeService: EmployeeService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [DepartmentService, DepartmentRepository, EmployeeService],
    }).compile();

    service = module.get<DepartmentService>(DepartmentService);
    repository = module.get<DepartmentRepository>(DepartmentRepository);
    employeeService = module.get<EmployeeService>(EmployeeService);
  });

  it('should save the Department by calling Repository', async () => {
    //Arrange
    const createDepartmentDto = new CreateDepartmentDto();
    createDepartmentDto.name = 'SHIELD';
    repository.save = jest.fn().mockResolvedValue(createDepartmentDto);

    //Act
    const result: Department = await service.save(createDepartmentDto);

    //Assert
    expect(repository.save).toBeCalledWith(createDepartmentDto);
    expect(result).toBe(createDepartmentDto);
  });

  it('should get existing department by id', async () => {
    //Arrange
    const departmentId = 'D1';
    repository.findById = jest
      .fn()
      .mockResolvedValue({ id: departmentId, name: 'SHIELD' });

    //Act
    const result: Department = await service.findById(departmentId);

    //Assert
    expect(repository.findById).toBeCalledWith(departmentId);
  });

  it('should get department with employees', async () => {
    //Arrange
    const departmentId = 'D1';
    repository.findById = jest
      .fn()
      .mockResolvedValue({ id: departmentId, name: 'SHIELD' });

    const employee1 = new Employee();
    employee1.name = 'John';
    employee1.departmentId = departmentId;
    employee1.createdAt = new Date();

    const employee2 = new Employee();
    employee2.name = 'Jane';
    employee2.departmentId = departmentId;
    employee2.createdAt = new Date();

    employeeService.find = jest.fn().mockResolvedValue([employee1, employee2]);

    //Act
    const departmentWithEmployees: DepartmentWithEmployees =
      await service.findByIdWithEmployees(departmentId);

    //Assert
    expect(repository.findById).toBeCalledWith(departmentId);
    expect(employeeService.find).toBeCalledWith(departmentId);
    expect(departmentWithEmployees.employees).toEqual([employee1, employee2]);
  });
});
