import { Test, TestingModule } from '@nestjs/testing';
import { DepartmentRepository } from './department.repository';
import { MongooseModule } from '@nestjs/mongoose';
import { MONGO_PORT } from '../../test/constants';
import { CreateDepartmentDto } from './dto/create-department.dto';
import { Department, DepartmentSchema } from './model/department';

describe('DepartmentRepository', () => {
  let departmentRepository: DepartmentRepository;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        MongooseModule.forRoot(`mongodb://localhost:${MONGO_PORT}`),
        MongooseModule.forFeature([
          { name: Department.name, schema: DepartmentSchema },
        ]),
      ],
      providers: [DepartmentRepository],
    }).compile();

    departmentRepository =
      module.get<DepartmentRepository>(DepartmentRepository);
  });

  it('should create department and return new id and createdAt', async () => {
    //Arrange
    const createDepartmentDto = new CreateDepartmentDto();
    createDepartmentDto.name = 'SHIELD';

    //Act
    const department: Department = await departmentRepository.save(
      createDepartmentDto,
    );

    //Assert
    expect(department.name).toBe(createDepartmentDto.name);
    expect(department.createdAt).toBeDefined();
  });

  it('should get an existing department by id', async () => {
    //Arrange
    const department: Department = await departmentRepository.save({
      name: 'SHIELD',
    });

    //Act
    const existingDepartment: Department = await departmentRepository.findById(
      department.id,
    );

    //Assert
    expect(existingDepartment).toBeDefined();
    expect(existingDepartment.id).toBe(department.id);
    expect(existingDepartment.name).toBe(department.name);
  });
});
