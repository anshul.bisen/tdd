import { DepartmentModule } from '../department.module';
import { CreateDepartmentDto } from '../dto/create-department.dto';
import { Department, DepartmentSchema } from '../model/department';
import { DepartmentService } from '../department.service';
import { DepartmentRepository } from '../department.repository';
import { EmployeeService } from '../../employee/employee.service';
import { EmployeeRepository } from '../../employee/employee.repository';
import { Employee, EmployeeSchema } from '../../employee/model/employee';
import mongoose from 'mongoose';
import { getModelToken } from '@nestjs/mongoose';

/** any module that the app uses should come here */
export const imports = [DepartmentModule];

export const mongoModels = [
  {
    provide: getModelToken('DepartmentModel'),
    useValue: mongoose.model('DepartmentModel', DepartmentSchema),
  },
  {
    provide: getModelToken('Employee'),
    useValue: mongoose.model('Employee', EmployeeSchema),
  },
];

/** any injectable should come here. example services, models, context, etc. */
export const providers = [
  CreateDepartmentDto,
  DepartmentService,
  DepartmentRepository,
  EmployeeService,
  EmployeeRepository,
  ...mongoModels,
];

export const COLLECTION_MAPPING = {
  department: {
    collection: Department,
    schema: DepartmentSchema,
    name: Department.name,
  },
  employee: {
    collection: Employee,
    schema: EmployeeSchema,
    name: Employee.name,
  },
};

export const mongoModelMap = new Map();
mongoModels.forEach((model) =>
  mongoModelMap.set(model.provide, model.useValue),
);
