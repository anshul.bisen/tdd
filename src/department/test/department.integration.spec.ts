import supertest from 'supertest';
import { INestApplication } from '@nestjs/common';
import { Test } from '@nestjs/testing';
import { DepartmentModule } from '../department.module';
import { CreateDepartmentDto } from '../dto/create-department.dto';
import { response } from 'express';

jest.mock('../department.service');
jest.mock('../department.repository');
jest.mock('./../../employee/employee.service');

describe('Departments', () => {
  let app: INestApplication;

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [DepartmentModule],
    }).compile();

    app = moduleRef.createNestApplication();
    await app.init();
  });

  it(`/POST departments`, () => {
    //Arrange
    const createDepartmentRequest: CreateDepartmentDto = {
      name: 'SHIELD',
    };

    //Act and Assert
    return supertest(app.getHttpServer())
      .post('/departments')
      .send(createDepartmentRequest)
      .expect(201);
  });

  it('/POST documents, returns 400 when department name is empty', () => {
    //Arrange
    const createDepartmentRequest: CreateDepartmentDto = {
      name: '',
    };

    //Act and Assert
    return supertest(app.getHttpServer())
      .post('/departments')
      .send(createDepartmentRequest)
      .expect(400)
      .then((response) => {
        expect(response.body.timestamp).toBeDefined();
        expect(response.body.path).toBe('/departments');
        expect(response.body.message).toBe('Bad Request');
      });
  });

  afterAll(async () => {
    await app.close();
  });
});
