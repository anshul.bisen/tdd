import supertest from 'supertest';
import { INestApplication, HttpStatus } from '@nestjs/common';
import mongoose from 'mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import { MongooseModule } from '@nestjs/mongoose';

import { CreateDepartmentDto } from '../dto/create-department.dto';
import { DepartmentService } from '../department.service';

import {
  providers,
  COLLECTION_MAPPING,
  mongoModelMap,
} from './dependency_provider';
import { DepartmentController } from '../department.controller';

describe('Departments', () => {
  let app: INestApplication;
  let server;
  let departmentService;
  const testDbUrl = `${process.env.TEST_DB_URL}/${process.env.TEST_DB_NAME}?directConnection=true`;

  beforeAll(async () => {
    await mongoose.connect(testDbUrl);
  });

  beforeEach(async () => {
    const schemaDefinitions = Object.values(COLLECTION_MAPPING).map(
      (schemaDef) => ({ name: schemaDef.name, schema: schemaDef.schema }),
    );

    /** create testing environment context*/
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        MongooseModule.forRoot(testDbUrl, {
          useNewUrlParser: true,
          useUnifiedTopology: true,
        }),
        MongooseModule.forFeature(schemaDefinitions),
      ],
      providers,
      controllers: [DepartmentController],
    }).compile();
    app = module.createNestApplication();

    departmentService = app.get<DepartmentService>(DepartmentService);
    await app.init();
    server = app.getHttpServer();

    /**
     * add seed data before each test, this should only load/set data for test in the describe block
     * the data loaded must be unloaded/unsetted after each test
     */
    const promises = [];
    promises.push(
      mongoModelMap.get('DepartmentModelModel').create({ name: 'SHIELD' }),
    );
    promises.push(
      mongoModelMap.get('DepartmentModelModel').create({ name: 'MIB' }),
    );

    await Promise.all(promises);
  });

  afterEach(async () => {
    const promises = [];
    promises.push(
      mongoModelMap.get('DepartmentModelModel').remove({ name: 'SHIELD' }),
    );
    promises.push(
      mongoModelMap.get('DepartmentModelModel').remove({ name: 'MIB' }),
    );
    await Promise.all(promises);
    /** server should be closed after each as a best practice */
  });

  it(`/POST departments`, async () => {
    //Arrange
    const createDepartmentRequest: CreateDepartmentDto = {
      name: 'IMF',
    };

    //Act
    const request = await supertest(app.getHttpServer())
      .post('/departments')
      .send(createDepartmentRequest);

    //Assert
    expect(request).toEqual(
      expect.objectContaining({ status: HttpStatus.CREATED }),
    );
  });

  it(`/GET departments by name`, async () => {
    //Arrange

    //Act
    const request = await supertest(app.getHttpServer()).get(
      '/departments/IMF',
    );

    //Assert
    expect(request).toEqual(
      expect.objectContaining({
        status: HttpStatus.OK,
        /** body should be an array  */
        body: expect.arrayContaining([
          /** array should have object, with name property, the value should be string */
          expect.objectContaining({ name: expect.any(String) }),
        ]),
      }),
    );
  });

  it(`/GET departments by name, when name of the department is string, it should pass`, async () => {
    //Arrange
    jest
      .spyOn(departmentService, 'findByName')
      .mockImplementationOnce(async () => [{ name: 'Avengers' }]);

    //Act
    const request = await supertest(app.getHttpServer()).get(
      '/departments/Avengeers',
    );

    //Assert
    expect(request).toEqual(
      expect.objectContaining({
        status: HttpStatus.OK,
        /** body should be an array  */
        body: expect.arrayContaining([
          /** array should have object, with name property, the value should be string */
          expect.objectContaining({ name: expect.any(String) }),
        ]),
      }),
    );
  });

  afterAll(async () => {
    server.close();
    await app.close();
  });
});
