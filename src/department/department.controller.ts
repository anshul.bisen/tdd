import { Body, Controller, Get, Post, UseFilters, Param } from '@nestjs/common';

import { CreateDepartmentDto } from './dto/create-department.dto';
import { DepartmentService } from './department.service';
import { CreateDepartmentDtoPipe } from './pipes/create-department-dto.pipe';
import { Department } from './model/department';
import { HttpExceptionFilter } from '../common/filters/http-exception-filter';

@Controller('departments')
@UseFilters(HttpExceptionFilter)
export class DepartmentController {
  constructor(private readonly departmentService: DepartmentService) {}

  @Post()
  save(
    @Body(CreateDepartmentDtoPipe) createDepartmentDto: CreateDepartmentDto,
  ): Promise<Department> {
    return this.departmentService.save(createDepartmentDto);
  }

  @Get()
  async findById(departmentId: string): Promise<Department> {
    return await this.departmentService.findById(departmentId);
  }

  @Get('/:name')
  async findByName(@Param('name') name): Promise<Department[]> {
    return await this.departmentService.findByName(name);
  }

  @Get('/:name/hod')
  async getHodOfDepartment(@Param('name') name): Promise<object> {
    console.log(
      'abc sndckdn getHodOfDepartment',
      name,
      await this.departmentService.getHodOfDepartment,
    );
    return { hod: await this.departmentService.getHodOfDepartment(name) };
  }
}
