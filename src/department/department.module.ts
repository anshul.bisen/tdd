import { Module } from '@nestjs/common';
import { DepartmentController } from './department.controller';
import { DepartmentService } from './department.service';
import { DepartmentRepository } from './department.repository';
import { EmployeeService } from './../employee/employee.service';

@Module({
  controllers: [DepartmentController],
  providers: [DepartmentService, DepartmentRepository, EmployeeService],
})
export class DepartmentModule {}
